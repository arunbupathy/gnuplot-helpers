# gnuplot-helpers

This repository provides a configuration file for gnuplot, along with a helper script that makes it easy to use gnuplot. It uses cairolatex terminal which allows you to include LaTeX expressions inside gnuplot. The helper script is a modification of [Petr Mikulik's `gpsavediff`](http://www.gnuplot.vt.edu/scripts/files/gpsavediff), which strips off the default commands from the save file produced by gnuplot, making the save function a lot more easy to work with.

An install script has been provided, but you need to add `$HOME/.local/bin` (if not already) to the list of search paths of your machine and also have a working LaTeX installation.

## Usage:

Let us open gnuplot in a terminal, and try to plot some curves:

    set xlabel '$\\theta$'
    set ylabel '$y(\\theta)$'
    plot sin(x) w lp t '$\sin\theta$'
    repl sin(2*x) w lp t '$\sin{2\theta}$'
    repl cos(x) w lp t '$\cos\theta$'
    repl cos(2*x) w lp t '$\cos{2\theta}$'

Things are looking crowded and ugly. So let us make more adjustments:

    set samples 50
    set key bottom left
    set key box opaque
    set xrange [0:pi]
    set xtics 0.25*pi
    set mxtics 2
    set ytics 0.5
    set mytics 2

The next couple of commands set the xtick labels in units of pi and widen the legend box so as to fit the keys (which is typically not necessary):

    set format x '%.2P$\pi$'
    set key width 4

Do not worry about how it looks in the GUI. This will look much better once we print to file, which is done with the following commands:

    fname='s_and_c'
    @cat
    repl
    @pub

The pre-defined variable *fname* is used to specify the file name (without extension). *cat* and *pub* are macros that do a bunch of things for you.

As seen below, this generates a beautiful PDF plot *and* a script (with given file name), that can be loaded to reproduce the figure.

![plot](images/s_and_c.png "A beautiful plot of sines and cosines."){width=59%}

Here is the same plot with a global scale of `1.3`, aspect ratio `1.3` and alternating open/filled symbols:

![plot](images/s_and_c_diff.png "Sines and cosines, but with different scales.")

### Notes:

- If there already exists a `.gp` script with the given file name in the current folder, the script does *not* overwrite it. This is intentional.

- This implies that any changes made to the plot from within the gnuplot terminal after the first creation of the `.gp` script are not saved!

- Thus any subsequent plot updates must necessarily be done in the `.gp` script itself and loaded in gnuplot with the `load` command.

- If your LaTeX macros are enclosed in double quotes, prepend an extra backslash. For instance: `set xlabel "$\\sqrt{n}$"`

- To scale the canvas equally in both `x` and `y` directions, set `scale` to a value of your liking. For instance: `scale = 1.5`

- The default aspect ratio of the plot is `1` which is more or less square. To change the aspect ratio set `aspect` to a value of your liking.

- To change the point size, set `psize` to a value of your liking. Similarly, to change line width, set `lwidth` to your liking. Default is 1 for both.

- The first four point types are open symbols, followed by their filled versions. To change to alternating open/filled symbols do: `@apt`

- Look into the supplied `gnuplot` file for further information, which you might want to edit to your liking.
